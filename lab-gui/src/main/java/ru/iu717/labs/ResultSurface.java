package ru.iu717.labs;

import lombok.val;
import org.jzy3d.analysis.AbstractAnalysis;
import org.jzy3d.chart.factories.AWTChartComponentFactory;
import org.jzy3d.colors.Color;
import org.jzy3d.colors.ColorMapper;
import org.jzy3d.colors.colormaps.ColorMapRainbow;
import org.jzy3d.maths.Coord3d;
import org.jzy3d.plot3d.builder.Builder;
import org.jzy3d.plot3d.rendering.canvas.Quality;

import java.util.ArrayList;
import java.util.List;

public class ResultSurface extends AbstractAnalysis {

    private double stepX;
    private double stepZ;
    private double[][] temps;

    public ResultSurface(double stepX, double stepZ, double[][] temps) {
        super();
        this.stepX = stepX;
        this.stepZ = stepZ;

        this.temps = temps;
    }

    private List<Coord3d> createCoords() {

        val coords = new ArrayList<Coord3d>();
        val nx = temps.length;
        val nz = temps[0].length;
        double curX = 0;
        for (int i = 0; i < nx; ++i, curX += stepX) {
            double curZ = 0;
            for (int j = 0; j < nz; ++j, curZ += stepZ) {
                val curT = temps[i][j];
                coords.add(new Coord3d(curZ, curX, curT));
            }
        }

        return coords;
    }

    @Override
    public void init() {

        val coords = createCoords();
        val surface = Builder.buildDelaunay(coords);
        surface.setColorMapper(new ColorMapper(
                new ColorMapRainbow(),
                surface.getBounds().getZmin(),
                surface.getBounds().getZmax(),
                new Color(1, 1, 1, .5f)));
        surface.setFaceDisplayed(true);
        surface.setWireframeDisplayed(false);

        // Create a chart
        chart = AWTChartComponentFactory.chart(Quality.Intermediate, getCanvasType());
        val axeLayout = chart.getAxeLayout();
        axeLayout.setXAxeLabel("Z");
        axeLayout.setZAxeLabel("T");
        axeLayout.setYAxeLabel("X");
        chart.getScene().getGraph().add(surface);
    }
}
