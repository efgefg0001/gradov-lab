package ru.iu717.labs;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import lombok.SneakyThrows;
import lombok.val;
import org.jzy3d.analysis.AnalysisLauncher;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class WindowController implements Initializable {

    @FXML
    private TextField f0TextField;

    @FXML
    private TextField ftTextField;

    @FXML
    private TextField uocTextField;

    @FXML
    private TextField alphaTextField;

    @FXML
    private TextField aTextField;

    @FXML
    private TextField bTextField;

    @FXML
    private TextField nxTextField;

    @FXML
    private TextField nzTextField;

    @FXML
    private Button solveButton;

    private LabSolver labSolver;

    @Override
    public void initialize(URL url, ResourceBundle res) {
        labSolver = new LabSolver();
    }

    @FXML
    @SneakyThrows
    void onActionSolveButton(ActionEvent event) {
        val nx = Integer.valueOf(nxTextField.getText());
        val b = Double.valueOf(bTextField.getText());
        val stepX = b/nx;
        val nz = Integer.valueOf(nzTextField.getText());
        val a = Double.valueOf(aTextField.getText());
        val stepZ = a/nz;
        val ft = Double.valueOf(ftTextField.getText());
        val alpha = Double.valueOf(alphaTextField.getText());
        val f0 = Double.valueOf(f0TextField.getText());

        labSolver.setVariable(nx, nz, stepZ, stepX, ft, alpha, f0);
        val result = labSolver.calculationThreads();
        AnalysisLauncher.open(new ResultSurface(stepX, stepZ, result));
    }
}
