package ru.iu717.labs;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import lombok.val;

public class Main extends Application {

    private String fxmlFile = "/fxml/Window.fxml";
    private int width = 400;
    private int height = 500;

    public static void main(String[] args) {
        try {
            launch(args);
        } catch (Exception error) {
            error.printStackTrace();
        }
    }

    private FXMLLoader createLoader() {
        val url = getClass().getResource(fxmlFile);
        return new FXMLLoader(url);
    }

    public void start(Stage stage) throws Exception {
        FXMLLoader loader = createLoader();
        stage.setTitle("GUI");
        Parent parent = loader.load();
        stage.setScene(new Scene(parent, width, height));

        stage.setOnCloseRequest( event -> {
            System.exit(0);
        });

        stage.show();

    }
}
