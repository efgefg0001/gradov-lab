package ru.iu717.labs;

import lombok.val;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class LabSolver {

    private double[][] y;
    private double[][] lambda;

    int Nx, Nz;
    double stepZ, stepX, Ft, alp, f0;
    double[][] tmp;
    int Height, Width;

    public void setVariable(int _Nx, int _Nz, double _stepZ, double _stepX,
                            double _Ft, double _alp, double _f0) {
        this.Nx = _Nx; this.Nz = _Nz;
        this.stepX = _stepX; this.stepZ = _stepZ;
        this.Ft = _Ft; this.alp = _alp; this.f0 = _f0;
        this.tmp = new double[Nx][Nz];

        Height = Nx * Nz;
        Width = Nx * Nz + 1;

        actions.add(this::loopOne);
        actions.add(this::loopTwo);
        actions.add(this::loopThree);
        actions.add(this::loopFour);
    }

    // init* можно вынести в отдельный класс со страческими методами
    private double[][] initParameters(double[][] matr, int Nx, int Nz) {
        for (int i = 0; i < Nx; i++)
            for (int j = 0; j < Nz; j++) {
                matr[i][j] = 0.134 * Math.pow(10,-1)*(1+4.35*Math.pow(10,-4)*Constants.U);
            }
        return matr;
    }

    private double[][] initZero(double[][] matr, int Nx, int Nz) {
        for (int i = 0; i < Nz*Nx; i++)
            for (int j = 0; j < Nx*Nz+1; j++)
                matr[i][j] = 0;
        return matr;
    }

    private double[][] initConst(double[][] matr, int Nx, int Nz) {
        for (int i = 0; i < Nx; i++)
            for (int j = 0; j < Nz; j++)
                matr[i][j] = Constants.U;
        return matr;
    }

    private double[] initConst(double[] matr, int n) {
        for (int j = 0; j < n; j++)
            matr[j] = Constants.U;
        return matr;
    }

    private void fillLambda(int i, int j, double val) {
        lambda[i][j] = 0.134 * Math.pow(10,-1)*(1+4.35*Math.pow(10,-4)*val);
    }

    private double[][] recalculateSolution(double[] arr, double[][] matr, int Nx, int Nz) {
        for (int i = 0; i < Nx*Nz; i++) {
            int i_normal = i / Nx, j_normal = i % Nx;
            matr[i_normal][j_normal] = arr[i];
        }
        return matr;
    }

    public static void output(double[][] m, int Nx, int Nz) {
        for (int i = 0; i < Nx*Nz; i++) {
            for (int j = 0; j < Nx*Nz+1; j++) {
                System.out.print(m[i][j]);
                System.out.print(' ');
                if ((j+1) % Nx == 0)
                    System.out.print(" ");
            }
            System.out.println();
        }
    }

    public void testGauss()
    {
        double [][]y = new double[9][10];
        double []sol = new double[9];
        for (int i=0; i<9; i++)
            for (int j=0; j<10; j++)
                y[i][j] = 1;

        sol = Methods.Gauss(y, 3, 3);
    }

    private List<Runnable> actions = new ArrayList<>();

    private void loopOne() {
        // 2
        for (int i = 0; i < Nx; i++) {
            y[i][i] = 1;
            y[i][i + Nx] = -1;
            y[i][Nx * Nz] = stepZ / lambda[i][i] * Ft;
        }

        // 5
        for (int j = Nx * (Nz - 1), i=0; j < Nx * Nz; i++,j++) {
            y[j][Nx * (Nz - 2) + i] = 1;
            y[j][Nx * (Nz - 1) + i] = -(1 + (alp * stepZ) / (lambda[i][Nx-1]));
            y[j][Nx * Nz] = -(alp * stepZ) / lambda[i][Nx-1] * Constants.U;
        }
    }
    private void loopTwo() {
        // 3
        for (int i = 1; i < Nx-1; i++) {
            y[i*Nx][i*Nx] = 1 - (alp * stepX) / lambda[0][i];
            y[i*Nx][i*Nx+1] = -1;
            y[i*Nx][Nx * Nz] = -((alp * stepX) / lambda[0][i]) * Constants.U;
        }
    }
    private void loopThree() {
        // 4
        for (int i = 1; i < Nx-1; i++) {
            y[(i+1)*Nx-1][Nx*(i+1)-2] = 1;
            y[(i+1)*Nx-1][Nx*(i+1)-1] = -(1 + alp * stepX / lambda[Nx-1][i]);
            y[(i+1)*Nx-1][Nx * Nz] = -(alp * stepX / lambda[Nx-1][i]) * Constants.U;
        }
    }
    private void loopFour() {
        // сновное уравнение (1)
        for (int i = Nx; i < Nx*(Nz-1); i++) {
            if (i % Nx == 0 || (i+1) % Nx == 0) continue;
            int i_normal = i / Nx;
            int j_normal = i % Nx;
            y[i][i - 1] = A(i_normal, j_normal, stepX);
            y[i][i + 1] = B(i_normal, j_normal, stepX);
            y[i][i] = C(i_normal, j_normal, stepX, stepZ);
            y[i][i + Nx] = E(i_normal, j_normal, stepZ);
            y[i][i - Nx] = D(i_normal, j_normal, stepZ);
            y[i][Nx * Nz] = f(f0, tmp[i_normal][j_normal], stepZ*j_normal);
            double sum = A(i_normal, j_normal, stepX) + B(i_normal, j_normal, stepX) + E(i_normal, j_normal, stepZ) + D(i_normal, j_normal, stepZ);
            double _C = -C(i_normal, j_normal, stepX, stepZ);
//            if (sum != _C)
//                System.out.println("fill matrix is ok");
        }
    }

    public double[][] calculationThreads() {
        double[][] tmp = new double[Nx][Nz];
        double[] sol = new double[Nx*Nz];
        y = new double[Nx*Nz][Nx*Nz+1];
        lambda = new double[Nx][Nz];

        tmp = initConst(tmp, Nx, Nz);
        lambda = initParameters(lambda, Nx, Nz);
        sol = initConst(sol, Nx*Nz);
        y = initZero(y, Nx, Nz);

        long startTime = 0;
        long endTime;
        long traceTime;
        ExecutorService executor = Executors.newFixedThreadPool(4);
        val taskList = new ArrayList<Callable<Runnable>>();

        double _t;
        do {
            tmp = recalculateSolution(sol, tmp, Nx, Nz);
            y = initZero(y, Nx, Nz);

            actions.forEach(action -> taskList.add(() -> {
                action.run();
                return null;
            }));

            try {
                startTime = System.nanoTime();
                executor.invokeAll(taskList);
            } catch (InterruptedException e) {
                System.out.println("tasks interrupted");
            } finally {
    //            executor.shutdown();
                endTime = System.nanoTime();
                traceTime = endTime - startTime;
                System.out.println("shutdown finished");
                System.out.println("time running threads: " + traceTime);
            }

//            System.out.println("\nUnder Gauss");
//            output(y, Nx, Nz);
            sol = GaussSystemSolver.lsolveGauss(y, Nx*Nz);
//            System.out.println("\n\nAfter Gauss");
//            output(y, Nx, Nz);

            // return temp
//            System.out.println("\nSolve");
//            System.out.println(iter++);
//            for (int i = 0; i < Nx*Nz; i++)
//                System.out.println(sol[i]);

            // пересчет лямбды
            for (int i = 0; i < Nx; i++) {
                int i_normal = i / Nx;
                for (int j = 0; j < Nz; j++) {
                    int j_normal = i % Nx;
                    fillLambda(i_normal, j_normal, tmp[i_normal][j_normal]);
                }
            }

            _t = Methods.maxDiff(sol, tmp, Nx, Nz);
        } while (_t > Constants.EPS);

        // конечный 'вектор' решений
/*        System.out.println("\nSolve");
        for (int i = 0; i < Nx; i++) {
            for (int j = 0; j < Nz; j++) {
                System.out.print(tmp[i][j]);
                System.out.print(' ');
            }
            System.out.println();
        }*/

        return tmp;
    }

    public void calculate() {
        double[][] tmp = new double[Nx][Nz];
        double[] sol = new double[Nx*Nz];
        y = new double[Nx*Nz][Nx*Nz+1];
        lambda = new double[Nx][Nz];

        tmp = initConst(tmp, Nx, Nz);
        lambda = initParameters(lambda, Nx, Nz);
        sol = initConst(sol, Nx*Nz);
        y = initZero(y, Nx, Nz);

        double _t;
        do {
            tmp = recalculateSolution(sol, tmp, Nx, Nz);
            y = initZero(y, Nx, Nz);

            //last col Nx * Nz
            loopOne();
            // System.out.println("\nquat 2, 5");
            // output(y, Nx, Nz);

            loopTwo();
          //  System.out.println("\nquat 3");
          //  output(y, Nx, Nz);

            loopThree();
           // System.out.println("\nquat 4");
           // output(y, Nx, Nz);

            loopFour();
            // System.out.println("\nUnder Gauss 1");
//            output(y, Nx, Nz);

//            System.out.println("\nUnder Gauss");
//            output(y, Nx, Nz);
            sol = GaussSystemSolver.lsolveGauss(y, Nx*Nz);
//            System.out.println("\n\nAfter Gauss");
//            output(y, Nx, Nz);

            // return temp
//            System.out.println("\nSolve");
//            System.out.println(iter++);
//            for (int i = 0; i < Nx*Nz; i++)
//                System.out.println(sol[i]);

            // пересчет лямбды
            for (int i = 0; i < Nx; i++) {
                int i_normal = i / Nx;
                for (int j = 0; j < Nz; j++) {
                    int j_normal = i % Nx;
                    fillLambda(i_normal, j_normal, tmp[i_normal][j_normal]);
                }
            }

            _t = Methods.maxDiff(sol, tmp, Nx, Nz);
        } while (_t > Constants.EPS);

        // конечный 'вектор' решений
        System.out.println("\nSolve");
        for (int i = 0; i < Nx; i++) {
            for (int j = 0; j < Nz; j++) {
                System.out.print(tmp[i][j]);
                System.out.print(' ');
            }
            System.out.println();
        }
    }

    private double K(double value) {
        if (value == 293)
            return 2*Math.pow(10,-3);
        else if (value > 293 && value < 1278)
            return interpolation(value, 293, 1278, 2*Math.pow(10,-3), 5*Math.pow(10,-3));
        else if (value == 1278)
            return 5*Math.pow(10,-3);
        else if (value > 1278 && value < 1528)
            return interpolation(value, 1278, 1528, 5*Math.pow(10,-3), 7.8*Math.pow(10,-3));
        else if (value == 1528)
            return 7.8*Math.pow(10,-3);
        else if (value > 1528 && value < 1677)
            return interpolation(value, 1528, 1677, 7.8*Math.pow(10,-3), 1*Math.pow(10,-2));
        else if (value == 1677)
            return 1*Math.pow(10,-2);
        return 0.0;
    }

    private double interpolation(double val, double x1, double x2, double y1, double y2) {
        double _y = y2-y1;
        double _x = x2 - x1;
        return (_y/_x)*(val-x1)+y1;
    }

    private double f(double F0, double t, double z) {
        return F0*Math.exp(-K(t)*z);
    }

    private double A(int i, int j, double step) {
        double labmda_2 = (lambda[i][j-1] + lambda[i][j]) / 2;
        return (labmda_2/Math.pow(step,2));
    }
    private double B(int i, int j, double step) {
        double labmda_2 = (lambda[i][j+1] + lambda[i][j]) / 2;
        return labmda_2/Math.pow(step,2);
    }
    private double C(int i, int j, double stepX, double stepZ) {
        double labmdaPlusJ = (lambda[i][j+1] + lambda[i][j]) / 2;
        double labmdaMinusJ = (lambda[i][j-1] + lambda[i][j]) / 2;
        double labmdaPlusI = (lambda[i+1][j] + lambda[i][j]) / 2;
        double labmdaMinusI = (lambda[i-1][j] + lambda[i][j]) / 2;
        return -((labmdaPlusJ + labmdaMinusJ)/Math.pow(stepX,2) +
                (labmdaPlusI + labmdaMinusI)/Math.pow(stepZ,2));
    }
    private double D(int i, int j, double stepZ) {
        double labmda_2 = (lambda[i-1][j] + lambda[i][j]) / 2;
        return labmda_2/Math.pow(stepZ,2);
    }
    private double E(int i, int j, double stepZ) {
        double labmda_2 = (lambda[i+1][j] + lambda[i][j]) / 2;
        return labmda_2/Math.pow(stepZ,2);
    }
}
