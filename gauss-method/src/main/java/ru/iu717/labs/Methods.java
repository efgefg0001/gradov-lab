package ru.iu717.labs;

import static java.lang.Math.abs;

public class Methods {
    public static double maxDiff(double[] sol, double[][] matr, int Nx, int Nz) {
        double max = Integer.MIN_VALUE;
        int index = -1;
        for (int i = 0; i < Nx * Nz - 1; i++) {
            int i_normal = i / Nx, j_normal = i % Nx;
            if (j_normal == 1 || j_normal == 2) continue;
            if (Math.abs(sol[i] - matr[i_normal][j_normal]) > max) {
                max = Math.abs(sol[i] - matr[i_normal][j_normal]);
                index = i;
            }
        }
        return max;
    }

    public static int getMaxInCurrentColumn(double[][] matrix, int Nx, int Nz, int col) {
        double max = Math.abs(matrix[col][col]);
        int index = col;
        for (int i = col + 1; i < Nz * Nx; i++) {
            if (abs(matrix[i][col]) > max) {
                max = abs(matrix[i][col]);
                index = i;
            }
        }
        return index;
    }

    public static void swapRows(double[][] matrix, int Nx, int Nz, int r1, int r2) {
        for (int j = 0; j <= Nx * Nz; j++) {
            double t = matrix[r1][j];
            matrix[r1][j] = matrix[r2][j];
            matrix[r2][j] = t;
        }
    }

    // http://prog-cpp.ru/gauss/
    public static double[] Gauss(double[][] matrix, int Nz, int Nx) {
        // matrix[i][Nx * Nz] - правая часть уравнения
        double[] x = new double[Nz * Nx]; // выходной вектор
        double max = 0;

        int k = 0, index = 0;

        while (k < (Nz * Nx)) {
            index = getMaxInCurrentColumn(matrix, Nx, Nz, k);

            if (abs(matrix[index][k]) < Constants.EPS)
                throw new NullPointerException("Solution not exist from zero column " + index);

            swapRows(matrix, Nx, Nz, k, index);
            double tmp2;

            // Normalization
            for (int i = k; i < Nx * Nz; i++) {
                double temp = matrix[i][k];
                if (temp < 0) {
                    tmp2 = abs(temp);
                    tmp2++;
                }
                if (abs(temp) < Constants.EPS)
                    continue; // для нулевого коэффициента пропустить
                for (int j = 0; j < Nx * Nz + 1; j++) {
                    matrix[i][j] /= temp;
                }
                //System.out.println("1");

                //LabSolver.output(matrix, Nx, Nz);
                // matrix[i][Nx * Nz] /= temp;
                if (i == k) continue; // уравнение не вычитать само из себя
                for (int j = 0; j < Nx * Nz + 1; j++)
                    matrix[i][j] -= matrix[k][j];
                //System.out.println("2");
                //LabSolver.output(matrix, Nx, Nz);
                //matrix[i][Nx * Nz] -= matrix[k][Nx * Nz];
            }
            //System.out.println("3");
            //LabSolver.output(matrix, Nx, Nz);
            k++;
        }

      //  System.out.println();
      //  System.out.println("====");
      //  LabSolver.output(matrix, Nx, Nz);
        //------Обратный ход


        for (k = Nx * Nz - 1; k >= 0; k--) {
            for (int i = 0; i < k; i++)
                matrix[i][Nx * Nz] = matrix[i][Nx * Nz] - matrix[i][k] * matrix[k][Nx * Nz];
        }
     /*   double tmp = 0.0;
        for (int i = Nx*Nz - 1; i>=0; i--)
        {
            tmp = matrix[i][Nx*Nz]; // b[i]
            for (int j = i+1; j<Nx*Nz; j++)
                tmp -= matrix[i][j]*x[j];
            x[i] = tmp / matrix[i][i];
        }*/
        for (int i = 0; i < Nx * Nz; i++)
            x[i] = matrix[i][Nx * Nz];

        return x;
    }
}
